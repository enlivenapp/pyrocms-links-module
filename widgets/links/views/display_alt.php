<ul id="links-widget">

<p> Go to section &#8594;  
<?php
/*
The array already comes in alphabetized.
This first part pulls the first character of the link's name, forces uppercase and 
creates the anchor pointer.  IE:
Go to -> A B C D E
*/

$exists = array();  // new array of links
?>
<?php foreach($links as $link): ?>
<?php
		$fc = strtoupper($link['link_name']); // upper case
		$fco = $fc[0];   // pain in my ass
		
// if the letter is not in the array...		
		if (!array_key_exists($fc[0], $exists) ) 
		{
			// print out the pointer to the anchor tag
?>
			<a href="<?php echo current_url(); ?>#<?php echo $fc[0]; ?>"><?php echo $fc[0]; ?></a> 
<?php	
		// add the single letter to the $exists array because it's not already there.
		$exists[$fc[0]] = array();
		$exists[$fco][] = $link;
		}
		else 
		{
			// since we already know that the array exists, we'll just add it to the proper array
			$fc = strtoupper($link['link_name']); // make sure it's uppercase
			$fco = $fc[0]; // Pain in my ass
			$exists[$fco][] = $link; // adding to the proper array
		}
?>		

 <?php endforeach; ?>
</p>
<!-- <p>Array Print: <pre><?php print_r($exists); ?>	</pre></p> -->

<?php foreach ($exists as $key => $link): ?>
<h4 class="underline">
	<span class="active"><a name="<?php echo $key ?>"></a><?php echo $key ?> </span></h4>
<p>
<ul>
	<?php foreach ($link as $item): ?>
	<li><a href="<?php echo $item['link_url']; ?>" title="<?php echo $item['link_name']; ?>" target="<?php echo $item['link_target']; ?>" ><?php echo $item['link_name']; ?></a></li>
<?php endforeach; ?>
</ul>	
</p>
<p style="margin-bottom: 75px;">
	<a href="#" id="to-top">[ ↑ Top ]</a></p>
<?php endforeach; ?>
