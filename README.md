# PyroCMS-Links-Module
====================

Package: Links Module for PyroCMS<br>
Author: Mike Webber<br>
Website: http://wildbluechicken.com<br>


## License: MIT<br>
#### Copyright (C) 2013 Michael Webber<br>

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and 
associated documentation files (the "Software"), to deal in the Software without restriction, including 
without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell 
copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the 
following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial 
portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT 
LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. 
IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, 
WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE 
SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
<hr>



## Installation:

1: Download and rename the package directory to "links"<br>
2: upload into /addons/default/modules/  :OR: /addons/shared_addons/modules/<br>
3: Activate in the CP under "addons".<br>
<hr>


## Description:

Places a blogroll type, alphabetized list of links in public pages using a widget. (alternate view available, see Alternate Display below)
<hr>


## Use:

In a page body:<br>
1: Install<br>
2: ACP --> Content --> Widgets<br>
3: Click "Areas" <br>
4: Create uniquely named new area<br>
5: Click "instances"<br>
6: drag "Links - show a list links" to your newly created area and configure<br>
7: copy code: {{ widgets:area slug="WHATEVER-YOU-NAMED-IT" }} and place in page<br>
8: APC --> Links<br>
9: Add links <br>

In a sidebar:<br>
1: drag "Links - show a list links" to your sidebar widget and configure<br>
2: APC --> Links<br>
3: Add links <br>
<hr>


## Alternate Display
In widgets/links/views/<br>

You'll find display.php and display_alt.php

display.php - default - The original display.  Creates an alphabetized unordered list

display_alt.php - Newly Added: Creates an alphabetized list of links with anchors:

"Go to section --> A B C D E F,  etc."  Only lists letters where there is a link item

(Header with anchor) A<br>
(Header with anchor) B<br>
(Header with anchor) C<br>
etc
<hr>


### To use:

 display.php : nothing (default behavior) <br>
 display_alt.php : rename display.php to something else and rename display_alt.php to display.php



## Issues or Bugs:
https://github.com/wbc-mike/PyroCMS-Links-Module/issues
<hr>

## Thanks:
Mike Van Winkle - Original Author - http://www.mikevanwinkle.com.
<hr>


## Changelog

1.0.3.1 - Bug fix, display_alt.php

1.0.3 - New Author release. 4 bug fixes and added alternate display

1.0.2 - Release by mikevanwinkle (bug fix?)

1.0.1 - initial release by mikevanwinkle